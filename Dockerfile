FROM maven:3.5.3-jdk-8

RUN apt update && apt -y install haskell-platform cabal-install

COPY sprockell /sprockell

RUN mkdir -p out/features

WORKDIR /sprockell
RUN cabal install

WORKDIR /opt/atlassian/pipelines/agent/build/
