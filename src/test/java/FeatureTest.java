/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

import exceptions.ParseException;
import exceptions.TypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * This class tests all individual features.
 * Programs from the 'src/test/java/programs' directory are subject to this class.
 */
public class FeatureTest extends BasicTest {

    /**
     * Prefix for the files in this test
     */
    private static final String prefix = "features/";
    /**
     * The programs to test in this test.
     */
    private static final String[] allFeatureTestPrograms = new String[]{
            prefix + "addition",
            prefix + "comment",
            prefix + "comp",
            prefix + "multiplication",
            prefix + "negative",
            prefix + "negative_2",
            prefix + "parallel",
            prefix + "parallelinparallel",
            prefix + "prefix",
            prefix + "scopes",
            prefix + "subtraction",
            prefix + "testandset",
            prefix + "while",
    };

    /**
     * This methods makes sure that all programs above are compiled into instructions.
     *
     * @throws IOException          Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     * @throws ParseException       Thrown when checking (parsing) the program leads to errors.
     * @throws InterruptedException Thrown when the compilation of Haskell (ghc) is interrupted.
     * @throws TypeException        Thrown when the type checker reports that there is a type mismatch.
     */
    @BeforeClass
    public static void compilePrograms() throws IOException, ParseException, InterruptedException, TypeException {
        compilePrograms(allFeatureTestPrograms);
    }

    /**
     * Test basic addition of two variables (whether the printed result is what we expect)
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void addition() throws IOException {
        assertEquals(getExpected(51), runHaskellProgram(prefix + "addition"));
    }

    /**
     * Test if a comment after a (assign)statement does not lead to any errors or weird behaviour.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void comment() throws IOException {
        assertEquals(getExpected(5), runHaskellProgram(prefix + "comment"));
    }

    /**
     * Test if all comparison statements work
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void comp() throws IOException {
        assertEquals(getExpected(1) +
                getExpected(2) +
                getExpected(3) +
                getExpected(4) +
                getExpected(5) +
                getExpected(6) +
                getExpected(7) +
                getExpected(8), runHaskellProgram(prefix + "comp"));
    }

    /**
     * Test if multiplication expression evaluates correctly and yields the expected result.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void multiplication() throws IOException {
        assertEquals(getExpected(6), runHaskellProgram(prefix + "multiplication"));
    }

    /**
     * Test negative numbers. Subtract two negative numbers and verify correct computation.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void negative() throws IOException {
        assertEquals(getExpected(1), runHaskellProgram(prefix + "negative"));
    }

    /**
     * Test negative numbers, again. This time with multiplication instead of subtraction.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void negative2() throws IOException {
        assertEquals(getExpected(-15), runHaskellProgram(prefix + "negative_2"));
    }

    /**
     * Test if code in parallel statement executes.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void parallel() throws IOException {
        assertEquals(getExpectedFrom(1, 4), runHaskellProgram(prefix + "parallel"));
    }

    /**
     * Test if code in nested parallel statements executes.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void parallelInParallel() throws IOException {
        assertEquals(getExpectedFrom(1, 1), runHaskellProgram(prefix + "parallelinparallel"));
    }

    /**
     * Test prefix expression: !false should be true and !true should be false.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void prefix() throws IOException {
        assertEquals(getExpected(1) + getExpected(0) + getExpected(1), runHaskellProgram(prefix + "prefix"));
    }

    /**
     * Test functionality of scopes. Variables declared in another scope should not overwrite variables of higher scope.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void scopes() throws IOException {
        assertEquals(getExpected(5), runHaskellProgram(prefix + "scopes"));
    }

    /**
     * Test basic subtraction operation.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void subtraction() throws IOException {
        assertEquals(getExpected(1), runHaskellProgram(prefix + "subtraction"));
    }

    /**
     * Test inter-thread communication.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void testAndSet() throws IOException {
        assertEquals(getExpected(5), runHaskellProgram(prefix + "testandset"));
    }

    /**
     * Test if code in loop runs multiple times and exactly the expected amount of times. It is tested with a very
     * big number to verify that there is no IndexOutOfBoundsException thrown at some point.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void whileLoop() throws IOException {
        assertEquals(getExpected(3300), runHaskellProgram(prefix + "while"));
    }

}
