/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

import compiler.Compiler;
import exceptions.ParseException;
import exceptions.TypeException;
import generator.Program;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This class contains all common methods for the tests. The tests extend this class in accordance to the DRY principle.
 */
class BasicTest {

    /**
     * Directory the test programs can be found
     */
    private final static String BASE_DIR = "src/test/java/programs/";
    /**
     * PROG file extension.
     */
    private final static String EXT = ".prog";

    /**
     * Compiles a list of programs
     *
     * @param progs List of the programs to compile.
     * @throws IOException          If one of the programs is not read or writable.
     * @throws ParseException       When one of the programs cannot be parsed.
     * @throws InterruptedException When a timeout occurs.
     * @throws TypeException        When a type error is found during type checking.
     */
    static void compilePrograms(String[] progs) throws IOException, ParseException, InterruptedException, TypeException {
        for (String prog : progs) {
            check(prog);
            compile(prog);
            compileHaskell(prog);
        }
    }

    /**
     * Compiles a program and writes it to disk with the same name in the 'out' directory
     *
     * @param name Program to compile.
     * @throws IOException    When there occurs an error with reading or writing the program
     * @throws ParseException When the file cannot be properly parsed.
     */
    private static void compile(String name) throws IOException, ParseException {
        Compiler compiler = Compiler.instance();
        compiler.flush();
        Program prog = compiler.compile(new File(BASE_DIR, name + EXT));
        compiler.writeProgram(prog, name);
    }

    /**
     * Execute ghc on the command line to compile given program.
     *
     * @param program Program to be compiled by ghc.
     * @throws IOException          when the program cannot be read.
     * @throws InterruptedException when a timeout occurred.
     */
    private static void compileHaskell(String program) throws IOException, InterruptedException {
        Runtime rt = Runtime.getRuntime();
        Process proc = rt.exec(new String[]{"ghc", String.format("out/%s.hs", program)});

        if (proc.waitFor() != 0) {
            System.err.println("Compiling went wrong!");
        }
    }

    /**
     * Type checks a program.
     *
     * @param name Program to check.
     * @throws IOException    When there is an error reading the program from disk.
     * @throws ParseException When there is an error parsing the file.
     * @throws TypeException  When the file cannot be properly type-checked.
     */
    static void check(String name) throws IOException, ParseException, TypeException {
        Compiler compiler = Compiler.instance();
        ParseTree tree = compiler.parse(new File(BASE_DIR, name + EXT));
        compiler.check(tree);
    }

    /**
     * Execute compiled Haskell program on the command line, while capturing the output.
     * The output is returned for verification.
     *
     * @param program Program to be compiled and run.
     * @return Result of the Haskell program.
     * @throws IOException when there is an error with executing the program.
     */
    String runHaskellProgram(String program) throws IOException {
        Runtime rt = Runtime.getRuntime();
        Process proc = rt.exec(new String[]{String.format("out/%s", program)});

        BufferedReader input = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        StringBuilder result = new StringBuilder();
        String s;
        while ((s = input.readLine()) != null) {
            result.append(s);
        }

        System.out.println(String.format("   Result of %s: %s", program, result.toString()));
        return result.toString();
    }

    /**
     * Helper to work with integerIO.
     *
     * @param i the integer to expect
     * @return String of expected value
     */
    String getExpected(int i) {
        return String.format("Sprockell 0 says %d", i);
    }

    /**
     * Helper to work with integerIO.
     *
     * @param i Sprockell to print it
     * @param j the integer to expect
     * @return String of expected value
     */
    String getExpectedFrom(int i, int j) {
        return String.format("Sprockell %d says %d", i, j);
    }

}
