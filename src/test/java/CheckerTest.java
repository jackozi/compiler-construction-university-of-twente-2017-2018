/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

import exceptions.ParseException;
import exceptions.TypeException;
import org.junit.Test;

import java.io.IOException;

public class CheckerTest extends BasicTest {

    /**
     * Prefix for the files to be tested in this test.
     */
    private static final String strongTypePrefix = "strongTyping/";

    /**
     * Verify that the basic program succeeds correctly.
     *
     * @throws IOException    Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     * @throws ParseException Thrown when checking (parsing) the program leads to errors.
     * @throws TypeException  Thrown when the type checker reports that there is a type mismatch.
     */
    @Test
    public void basic() throws IOException, ParseException, TypeException {
        check("basic");
    }

    /**
     * Verify a ParseException is thrown when an incorrect program is run through the parser.
     *
     * @throws IOException    Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     * @throws ParseException Thrown when checking (parsing) the program leads to errors.
     * @throws TypeException  Thrown when the type checker reports that there is a type mismatch.
     */
    @Test(expected = ParseException.class)
    public void parseException() throws IOException, ParseException, TypeException {
        check("parseException");
    }

    /**
     * Verify a TypeException is thrown when an undeclared variable is used.
     *
     * @throws IOException    Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     * @throws ParseException Thrown when checking (parsing) the program leads to errors.
     * @throws TypeException  Thrown when the type checker reports that there is a type mismatch.
     */
    @Test(expected = TypeException.class)
    public void undeclared() throws IOException, ParseException, TypeException {
        check("undeclared");
    }

    /**
     * Verify a TypeException is thrown when a variable is declared in the same scope but with a different type. (StrongTyping)
     *
     * @throws IOException    Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     * @throws ParseException Thrown when checking (parsing) the program leads to errors.
     * @throws TypeException  Thrown when the type checker reports that there is a type mismatch.
     */
    @Test(expected = TypeException.class)
    public void strongTypingSameScope() throws IOException, ParseException, TypeException {
        check(strongTypePrefix + "sameScope");
    }

    /**
     * Verify <b>no</b> TypeException is thrown when a variable is declared with 'init' in <b>a different</b> scope. (StrongTyping)
     *
     * @throws IOException    Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     * @throws ParseException Thrown when checking (parsing) the program leads to errors.
     * @throws TypeException  Thrown when the type checker reports that there is a type mismatch.
     */
    @Test
    public void strongTypingSameScopeInit() throws IOException, ParseException, TypeException {
        check(strongTypePrefix + "sameScopeInit");
    }

    /**
     * Verify a TypeException is thrown when a variable is declared in a different scope with a different type. (StrongTyping)
     *
     * @throws IOException    Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     * @throws ParseException Thrown when checking (parsing) the program leads to errors.
     * @throws TypeException  Thrown when the type checker reports that there is a type mismatch.
     */
    @Test(expected = TypeException.class)
    public void strongTypingDifferentScope() throws IOException, ParseException, TypeException {
        check(strongTypePrefix + "differentScope");
    }

    /**
     * Verify <b>no</b> TypeException is thrown when a variable is declared with 'init' in a different scope with a different type. (StrongTyping)
     *
     * @throws IOException    Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     * @throws ParseException Thrown when checking (parsing) the program leads to errors.
     * @throws TypeException  Thrown when the type checker reports that there is a type mismatch.
     */
    @Test
    public void strongTypingDifferentScopeInit() throws IOException, ParseException, TypeException {
        check(strongTypePrefix + "differentScopeInit");
    }

    /**
     * Verify a TypeException is thrown when a variable is declared with a different type, in a parallel statement.
     *
     * @throws IOException    Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     * @throws ParseException Thrown when checking (parsing) the program leads to errors.
     * @throws TypeException  Thrown when the type checker reports that there is a type mismatch.
     */
    @Test(expected = TypeException.class)
    public void strongTypingParallel() throws IOException, ParseException, TypeException {
        check(strongTypePrefix + "parallel");
    }

    /**
     * Verify a TypeException is thrown when a variable is declared with a different type, in the body of a while-statement.
     *
     * @throws IOException    Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     * @throws ParseException Thrown when checking (parsing) the program leads to errors.
     * @throws TypeException  Thrown when the type checker reports that there is a type mismatch.
     */
    @Test(expected = TypeException.class)
    public void strongTypingWhile() throws IOException, ParseException, TypeException {
        check(strongTypePrefix + "while");
    }

}
