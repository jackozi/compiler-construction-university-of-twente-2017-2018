/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

import exceptions.ParseException;
import exceptions.TypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * This class tests the generator and is used to verify the whole compiler and outputted code.
 * The compiled programs are run and the output is matched against the expected output.
 */
public class GeneratorTest extends BasicTest {

    /**
     * The programs to test in this test
     */
    private static final String[] allProgs = new String[]{
            "basic",
            "banking",
            "banking2",
            "peterson",
            "print",
    };

    /**
     * Makes sure all programs are compiled before the tests are run.
     *
     * @throws IOException          Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     * @throws ParseException       Thrown when checking (parsing) the program leads to errors.
     * @throws InterruptedException Thrown when the compilation of Haskell (ghc) is interrupted.
     * @throws TypeException        Thrown when the type checker reports that there is a type mismatch.
     * @see #compilePrograms
     */
    @BeforeClass
    public static void compilePrograms() throws IOException, ParseException, InterruptedException, TypeException {
        compilePrograms(allProgs);
    }

    /**
     * Test the banking program. This tests that editing an account in parallel yields the proper result, even though
     * it is accessed and updated in parallel.
     * It uses three 'locks' or 'barriers'. The first lock (barrier) is to make sure all
     * variables are defined before the parallel code is executed. Then there are locks in each
     * parallel block to make sure the final print statement is not executed before both parallel
     * executions are finished. This uses the 'makeFor' keyword.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void banking() throws IOException {
        assertEquals(getExpected(24), runHaskellProgram("banking"));
    }

    /**
     * Test the banking2 program.
     * This will test concurrency by initiating an account and in parallel adding some money to it till the first paymen can be made.
     * first thread should halt till the account is full enough to proceed.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void banking2() throws IOException {
        assertEquals(getExpected(1), runHaskellProgram("banking2"));
    }

    /**
     * Test the 'basic' program. This concludes if basic features of the Prog language work correctly.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void basic() throws IOException {
        assertEquals(getExpected(-11), runHaskellProgram("basic"));
    }

    /**
     * Test the 'print' program.
     * This concludes if printing (multiple times, in a loop and an if-statement) works correctly.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void print() throws IOException {
        assertEquals(getExpected(2) + getExpected(3), runHaskellProgram("print"));
    }

    /**
     * Test the 'peterson' program. This is a program based on Peterson's Algorithm.
     * This test verifies that mutual exclusion works correctly.
     * I.e., that two processes share a single-use resource without conflict, using only memory for communication.
     * It also demonstrates the use of a barrier and locks (`waitFor'-statement). The use of this statement is needed
     * for the program to run correctly, as there needs to be a barrier before the code in the parallel block can execute,
     * otherwise the global variables are not yet defines when the (parallel) code is run.
     * Also the final print statement can only be run when all parallel executions have finished. By using waitFor again,
     * this program demonstrates a basic lock implementation.
     *
     * @throws IOException Thrown when IO goes wrong (read-, write errors, disk faults, file not found etc.)
     */
    @Test
    public void petersonsAlgorithm() throws IOException {
        assertEquals("Sprockell 0 says 2", runHaskellProgram("peterson"));
    }

}