/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

// Generated from /Users/jackozi/mod8/cc-project-1718/src/main/java/grammar/grammar.g4 by ANTLR 4.7
package grammar;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.LexerATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GrammarLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		PROGRAM=1, TRUE=2, FALSE=3, AND=4, OR=5, IF=6, ELSE=7, END=8, VAR=9, WHILE=10, 
		DO=11, PRINT=12, PAR=13, WAITFOR=14, INIT=15, PLUS=16, NOT=17, LB=18, 
		RB=19, LPAR=20, RPAR=21, COMMA=22, DOT=23, DQUOTE=24, MINUS=25, COLON=26, 
		SEMIC=27, SLASH=28, STAR=29, ASS=30, EQ=31, NE=32, LE=33, LT=34, GE=35, 
		GT=36, ID=37, NUM=38, COMMENT=39, WS=40;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"PROGRAM", "TRUE", "FALSE", "AND", "OR", "IF", "ELSE", "END", "VAR", "WHILE", 
		"DO", "PRINT", "PAR", "WAITFOR", "INIT", "PLUS", "NOT", "LB", "RB", "LPAR", 
		"RPAR", "COMMA", "DOT", "DQUOTE", "MINUS", "COLON", "SEMIC", "SLASH", 
		"STAR", "ASS", "EQ", "NE", "LE", "LT", "GE", "GT", "ID", "NUM", "LETTER", 
		"DIGIT", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", 
		"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "COMMENT", 
		"WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, "'+'", "'!'", "'['", "']'", "'('", "')'", "','", 
		"'.'", "'\"'", "'-'", "':'", "';'", "'/'", "'*'", "'='", "'=='", "'!='", 
		"'<='", "'<'", "'>='", "'>'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "PROGRAM", "TRUE", "FALSE", "AND", "OR", "IF", "ELSE", "END", "VAR", 
		"WHILE", "DO", "PRINT", "PAR", "WAITFOR", "INIT", "PLUS", "NOT", "LB", 
		"RB", "LPAR", "RPAR", "COMMA", "DOT", "DQUOTE", "MINUS", "COLON", "SEMIC", 
		"SLASH", "STAR", "ASS", "EQ", "NE", "LE", "LT", "GE", "GT", "ID", "NUM", 
		"COMMENT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public GrammarLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "grammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2*\u0159\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5"+
		"\3\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3"+
		"\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3"+
		"\20\3\20\3\20\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3"+
		"\26\3\26\3\27\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\34\3\34\3"+
		"\35\3\35\3\36\3\36\3\37\3\37\3 \3 \3 \3!\3!\3!\3\"\3\"\3\"\3#\3#\3$\3"+
		"$\3$\3%\3%\3&\3&\3&\7&\u0107\n&\f&\16&\u010a\13&\3\'\6\'\u010d\n\'\r\'"+
		"\16\'\u010e\3(\3(\3)\3)\3*\3*\3+\3+\3,\3,\3-\3-\3.\3.\3/\3/\3\60\3\60"+
		"\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65\3\66\3\66\3\67\3\67"+
		"\38\38\39\39\3:\3:\3;\3;\3<\3<\3=\3=\3>\3>\3?\3?\3@\3@\3A\3A\3B\3B\3C"+
		"\3C\3D\3D\3D\7D\u014c\nD\fD\16D\u014f\13D\3D\3D\3E\6E\u0154\nE\rE\16E"+
		"\u0155\3E\3E\2\2F\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31"+
		"\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65"+
		"\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O\2Q\2S\2U\2W\2Y\2[\2]\2_\2a\2c\2"+
		"e\2g\2i\2k\2m\2o\2q\2s\2u\2w\2y\2{\2}\2\177\2\u0081\2\u0083\2\u0085\2"+
		"\u0087)\u0089*\3\2 \4\2C\\c|\3\2\62;\4\2CCcc\4\2DDdd\4\2EEee\4\2FFff\4"+
		"\2GGgg\4\2HHhh\4\2IIii\4\2JJjj\4\2KKkk\4\2LLll\4\2MMmm\4\2NNnn\4\2OOo"+
		"o\4\2PPpp\4\2QQqq\4\2RRrr\4\2SSss\4\2TTtt\4\2UUuu\4\2VVvv\4\2WWww\4\2"+
		"XXxx\4\2YYyy\4\2ZZzz\4\2[[{{\4\2\\\\||\4\2\f\f\17\17\5\2\13\f\17\17\""+
		"\"\2\u0141\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2"+
		"\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27"+
		"\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2"+
		"\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2"+
		"\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2"+
		"\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2"+
		"\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2\u0087\3\2\2\2\2\u0089\3"+
		"\2\2\2\3\u008b\3\2\2\2\5\u0093\3\2\2\2\7\u0098\3\2\2\2\t\u009e\3\2\2\2"+
		"\13\u00a2\3\2\2\2\r\u00a5\3\2\2\2\17\u00a8\3\2\2\2\21\u00ad\3\2\2\2\23"+
		"\u00b1\3\2\2\2\25\u00b5\3\2\2\2\27\u00bb\3\2\2\2\31\u00be\3\2\2\2\33\u00c4"+
		"\3\2\2\2\35\u00c8\3\2\2\2\37\u00d0\3\2\2\2!\u00d5\3\2\2\2#\u00d7\3\2\2"+
		"\2%\u00d9\3\2\2\2\'\u00db\3\2\2\2)\u00dd\3\2\2\2+\u00df\3\2\2\2-\u00e1"+
		"\3\2\2\2/\u00e3\3\2\2\2\61\u00e5\3\2\2\2\63\u00e7\3\2\2\2\65\u00e9\3\2"+
		"\2\2\67\u00eb\3\2\2\29\u00ed\3\2\2\2;\u00ef\3\2\2\2=\u00f1\3\2\2\2?\u00f3"+
		"\3\2\2\2A\u00f6\3\2\2\2C\u00f9\3\2\2\2E\u00fc\3\2\2\2G\u00fe\3\2\2\2I"+
		"\u0101\3\2\2\2K\u0103\3\2\2\2M\u010c\3\2\2\2O\u0110\3\2\2\2Q\u0112\3\2"+
		"\2\2S\u0114\3\2\2\2U\u0116\3\2\2\2W\u0118\3\2\2\2Y\u011a\3\2\2\2[\u011c"+
		"\3\2\2\2]\u011e\3\2\2\2_\u0120\3\2\2\2a\u0122\3\2\2\2c\u0124\3\2\2\2e"+
		"\u0126\3\2\2\2g\u0128\3\2\2\2i\u012a\3\2\2\2k\u012c\3\2\2\2m\u012e\3\2"+
		"\2\2o\u0130\3\2\2\2q\u0132\3\2\2\2s\u0134\3\2\2\2u\u0136\3\2\2\2w\u0138"+
		"\3\2\2\2y\u013a\3\2\2\2{\u013c\3\2\2\2}\u013e\3\2\2\2\177\u0140\3\2\2"+
		"\2\u0081\u0142\3\2\2\2\u0083\u0144\3\2\2\2\u0085\u0146\3\2\2\2\u0087\u0148"+
		"\3\2\2\2\u0089\u0153\3\2\2\2\u008b\u008c\5q9\2\u008c\u008d\5u;\2\u008d"+
		"\u008e\5o8\2\u008e\u008f\5_\60\2\u008f\u0090\5u;\2\u0090\u0091\5S*\2\u0091"+
		"\u0092\5k\66\2\u0092\4\3\2\2\2\u0093\u0094\5y=\2\u0094\u0095\5u;\2\u0095"+
		"\u0096\5{>\2\u0096\u0097\5[.\2\u0097\6\3\2\2\2\u0098\u0099\5]/\2\u0099"+
		"\u009a\5S*\2\u009a\u009b\5i\65\2\u009b\u009c\5w<\2\u009c\u009d\5[.\2\u009d"+
		"\b\3\2\2\2\u009e\u009f\5S*\2\u009f\u00a0\5m\67\2\u00a0\u00a1\5Y-\2\u00a1"+
		"\n\3\2\2\2\u00a2\u00a3\5o8\2\u00a3\u00a4\5u;\2\u00a4\f\3\2\2\2\u00a5\u00a6"+
		"\5c\62\2\u00a6\u00a7\5]/\2\u00a7\16\3\2\2\2\u00a8\u00a9\5[.\2\u00a9\u00aa"+
		"\5i\65\2\u00aa\u00ab\5w<\2\u00ab\u00ac\5[.\2\u00ac\20\3\2\2\2\u00ad\u00ae"+
		"\5[.\2\u00ae\u00af\5m\67\2\u00af\u00b0\5Y-\2\u00b0\22\3\2\2\2\u00b1\u00b2"+
		"\5}?\2\u00b2\u00b3\5S*\2\u00b3\u00b4\5u;\2\u00b4\24\3\2\2\2\u00b5\u00b6"+
		"\5\177@\2\u00b6\u00b7\5a\61\2\u00b7\u00b8\5c\62\2\u00b8\u00b9\5i\65\2"+
		"\u00b9\u00ba\5[.\2\u00ba\26\3\2\2\2\u00bb\u00bc\5Y-\2\u00bc\u00bd\5o8"+
		"\2\u00bd\30\3\2\2\2\u00be\u00bf\5q9\2\u00bf\u00c0\5u;\2\u00c0\u00c1\5"+
		"c\62\2\u00c1\u00c2\5m\67\2\u00c2\u00c3\5y=\2\u00c3\32\3\2\2\2\u00c4\u00c5"+
		"\5q9\2\u00c5\u00c6\5S*\2\u00c6\u00c7\5u;\2\u00c7\34\3\2\2\2\u00c8\u00c9"+
		"\5\177@\2\u00c9\u00ca\5S*\2\u00ca\u00cb\5c\62\2\u00cb\u00cc\5y=\2\u00cc"+
		"\u00cd\5]/\2\u00cd\u00ce\5o8\2\u00ce\u00cf\5u;\2\u00cf\36\3\2\2\2\u00d0"+
		"\u00d1\5c\62\2\u00d1\u00d2\5m\67\2\u00d2\u00d3\5c\62\2\u00d3\u00d4\5y"+
		"=\2\u00d4 \3\2\2\2\u00d5\u00d6\7-\2\2\u00d6\"\3\2\2\2\u00d7\u00d8\7#\2"+
		"\2\u00d8$\3\2\2\2\u00d9\u00da\7]\2\2\u00da&\3\2\2\2\u00db\u00dc\7_\2\2"+
		"\u00dc(\3\2\2\2\u00dd\u00de\7*\2\2\u00de*\3\2\2\2\u00df\u00e0\7+\2\2\u00e0"+
		",\3\2\2\2\u00e1\u00e2\7.\2\2\u00e2.\3\2\2\2\u00e3\u00e4\7\60\2\2\u00e4"+
		"\60\3\2\2\2\u00e5\u00e6\7$\2\2\u00e6\62\3\2\2\2\u00e7\u00e8\7/\2\2\u00e8"+
		"\64\3\2\2\2\u00e9\u00ea\7<\2\2\u00ea\66\3\2\2\2\u00eb\u00ec\7=\2\2\u00ec"+
		"8\3\2\2\2\u00ed\u00ee\7\61\2\2\u00ee:\3\2\2\2\u00ef\u00f0\7,\2\2\u00f0"+
		"<\3\2\2\2\u00f1\u00f2\7?\2\2\u00f2>\3\2\2\2\u00f3\u00f4\7?\2\2\u00f4\u00f5"+
		"\7?\2\2\u00f5@\3\2\2\2\u00f6\u00f7\7#\2\2\u00f7\u00f8\7?\2\2\u00f8B\3"+
		"\2\2\2\u00f9\u00fa\7>\2\2\u00fa\u00fb\7?\2\2\u00fbD\3\2\2\2\u00fc\u00fd"+
		"\7>\2\2\u00fdF\3\2\2\2\u00fe\u00ff\7@\2\2\u00ff\u0100\7?\2\2\u0100H\3"+
		"\2\2\2\u0101\u0102\7@\2\2\u0102J\3\2\2\2\u0103\u0108\5O(\2\u0104\u0107"+
		"\5O(\2\u0105\u0107\5Q)\2\u0106\u0104\3\2\2\2\u0106\u0105\3\2\2\2\u0107"+
		"\u010a\3\2\2\2\u0108\u0106\3\2\2\2\u0108\u0109\3\2\2\2\u0109L\3\2\2\2"+
		"\u010a\u0108\3\2\2\2\u010b\u010d\5Q)\2\u010c\u010b\3\2\2\2\u010d\u010e"+
		"\3\2\2\2\u010e\u010c\3\2\2\2\u010e\u010f\3\2\2\2\u010fN\3\2\2\2\u0110"+
		"\u0111\t\2\2\2\u0111P\3\2\2\2\u0112\u0113\t\3\2\2\u0113R\3\2\2\2\u0114"+
		"\u0115\t\4\2\2\u0115T\3\2\2\2\u0116\u0117\t\5\2\2\u0117V\3\2\2\2\u0118"+
		"\u0119\t\6\2\2\u0119X\3\2\2\2\u011a\u011b\t\7\2\2\u011bZ\3\2\2\2\u011c"+
		"\u011d\t\b\2\2\u011d\\\3\2\2\2\u011e\u011f\t\t\2\2\u011f^\3\2\2\2\u0120"+
		"\u0121\t\n\2\2\u0121`\3\2\2\2\u0122\u0123\t\13\2\2\u0123b\3\2\2\2\u0124"+
		"\u0125\t\f\2\2\u0125d\3\2\2\2\u0126\u0127\t\r\2\2\u0127f\3\2\2\2\u0128"+
		"\u0129\t\16\2\2\u0129h\3\2\2\2\u012a\u012b\t\17\2\2\u012bj\3\2\2\2\u012c"+
		"\u012d\t\20\2\2\u012dl\3\2\2\2\u012e\u012f\t\21\2\2\u012fn\3\2\2\2\u0130"+
		"\u0131\t\22\2\2\u0131p\3\2\2\2\u0132\u0133\t\23\2\2\u0133r\3\2\2\2\u0134"+
		"\u0135\t\24\2\2\u0135t\3\2\2\2\u0136\u0137\t\25\2\2\u0137v\3\2\2\2\u0138"+
		"\u0139\t\26\2\2\u0139x\3\2\2\2\u013a\u013b\t\27\2\2\u013bz\3\2\2\2\u013c"+
		"\u013d\t\30\2\2\u013d|\3\2\2\2\u013e\u013f\t\31\2\2\u013f~\3\2\2\2\u0140"+
		"\u0141\t\32\2\2\u0141\u0080\3\2\2\2\u0142\u0143\t\33\2\2\u0143\u0082\3"+
		"\2\2\2\u0144\u0145\t\34\2\2\u0145\u0084\3\2\2\2\u0146\u0147\t\35\2\2\u0147"+
		"\u0086\3\2\2\2\u0148\u0149\59\35\2\u0149\u014d\59\35\2\u014a\u014c\n\36"+
		"\2\2\u014b\u014a\3\2\2\2\u014c\u014f\3\2\2\2\u014d\u014b\3\2\2\2\u014d"+
		"\u014e\3\2\2\2\u014e\u0150\3\2\2\2\u014f\u014d\3\2\2\2\u0150\u0151\bD"+
		"\2\2\u0151\u0088\3\2\2\2\u0152\u0154\t\37\2\2\u0153\u0152\3\2\2\2\u0154"+
		"\u0155\3\2\2\2\u0155\u0153\3\2\2\2\u0155\u0156\3\2\2\2\u0156\u0157\3\2"+
		"\2\2\u0157\u0158\bE\2\2\u0158\u008a\3\2\2\2\b\2\u0106\u0108\u010e\u014d"+
		"\u0155\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}