grammar Grammar;

// Program
program : PROGRAM ID body EOF ;

// Body of a program (exists of zero or more statements)
body : statement*;

statement : asgnStmnt                         #assign
          | IF expr DO body (ELSE body)? END  #condition
          | WHILE expr DO body END            #while
          | PRINT expr                        #print
          | PAR body END                      #parallel
          | WAITFOR expr                      #wait
          ;

// Statements
asgnStmnt : ID ASS expr                       #assignment
          | INIT ID ASS expr                  #initAssignment
          ;

// Expressions
expr : prefixOp expr                          #prefixExpr
     | expr multOp expr                       #multExpr
     | expr plusOp expr                       #plusExpr
     | expr compOp expr                       #compExpr
     | expr boolOp expr                       #boolExpr
     | asgnStmnt                              #asgnAsExpr
     | LPAR expr RPAR                         #parExpr
     | NUM                                    #numExpr
     | ID                                     #idExpr
     | TRUE                                   #trueExpr
     | FALSE                                  #falseExpr
     ;

// Operations
prefixOp : MINUS | NOT ;
multOp : STAR | SLASH ;
plusOp : PLUS | MINUS ;
boolOp : AND | OR ;
compOp : EQ | NE | LE | LT | GE | GT ;

// Keywords
PROGRAM : P R O G R A M ;
TRUE    : T R U E ;
FALSE   : F A L S E ;
AND     : A N D ;
OR      : O R ;
IF      : I F ;
ELSE    : E L S E ;
END     : E N D ;
VAR     : V A R ;
WHILE   : W H I L E ;
DO      : D O ;
PRINT   : P R I N T ;
PAR     : P A R ;
WAITFOR : W A I T F O R ;
INIT    : I N I T ;

PLUS   : '+' ;
NOT    : '!' ;
LB     : '[' ;
RB     : ']' ;
LPAR   : '(' ;
RPAR   : ')' ;
COMMA  : ',' ;
DOT    : '.' ;
DQUOTE : '"' ;
MINUS  : '-' ;
COLON  : ':' ;
SEMIC  : ';' ;
SLASH  : '/' ;
STAR   : '*' ;
ASS    : '=' ;


// Compare operators
EQ : '==' ;
NE : '!=' ;
LE : '<=' ;
LT : '<'  ;
GE : '>=' ;
GT : '>'  ;

ID: LETTER (LETTER | DIGIT)* ;
NUM: DIGIT+ ;
fragment LETTER: [a-zA-Z] ;
fragment DIGIT: [0-9] ;

// Letter fragments (to make sure there is no distinction between uppercase and lowercase letters)
fragment A: [aA];
fragment B: [bB];
fragment C: [cC];
fragment D: [dD];
fragment E: [eE];
fragment F: [fF];
fragment G: [gG];
fragment H: [hH];
fragment I: [iI];
fragment J: [jJ];
fragment K: [kK];
fragment L: [lL];
fragment M: [mM];
fragment N: [nN];
fragment O: [oO];
fragment P: [pP];
fragment Q: [qQ];
fragment R: [rR];
fragment S: [sS];
fragment T: [tT];
fragment U: [uU];
fragment V: [vV];
fragment W: [wW];
fragment X: [xX];
fragment Y: [yY];
fragment Z: [zZ];

// Skipped token types
COMMENT : SLASH SLASH ~( '\r' | '\n')* -> skip;
WS : [ \t\r\n]+ -> skip;
