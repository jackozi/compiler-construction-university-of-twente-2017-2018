/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package compiler;

import checker.Checker;
import checker.Result;
import exceptions.ParseException;
import exceptions.TypeException;
import generator.Generator;
import generator.Program;
import grammar.GrammarLexer;
import grammar.GrammarParser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.*;

/**
 * This class has a singleton compiler to be used for compiling (checking and generating) provided code
 */
public class Compiler {
    /**
     * The singleton instance of this class.
     */
    private final static Compiler instance = new Compiler();
    /**
     * The checker of this compiler.
     */
    private Checker checker;
    /**
     * The generator of this compiler.
     */
    private Generator generator;
    /**
     * The compiler constructor. Can be private because we are working with the singleton-pattern.
     */
    private Compiler() {
        this.checker = new Checker();
        this.generator = new Generator();
    }

    /**
     * Returns the singleton instance of this class.
     *
     * @return Singleton of this class
     */
    public static Compiler instance() {
        return instance;
    }

    /**
     * Resets the checker and generator of this singleton compiler
     * only used for debugging purposes.
     */
    public void flush() {
        this.checker = new Checker();
        this.generator = new Generator();
    }

    /**
     * Type checks a given Simple Prog file
     *
     * @param file A file to type check
     * @return A result class containing details about this program
     * @throws ParseException When the file cannot be properly parsed
     * @throws IOException    When the file cannot be properly read
     * @throws TypeException  When the file contains type errors.
     */
    public Result check(File file) throws ParseException, IOException, TypeException {
        return check(parse(file));
    }

    /**
     * Type checks a given Simple Prog parse tree.
     *
     * @param tree ParseTree to check
     * @return A result class containing details about the program
     * @throws TypeException when there are type errors in the parseTree
     */
    public Result check(ParseTree tree) throws TypeException {
        checker.check(tree);
        if (checker.hasErrors()) {
            throw new TypeException(checker.getErrors());
        }
        return checker.check(tree);
    }

    /**
     * Compiles a given Simple Prog string into a parse tree.
     *
     * @param file File to parse to a parseTree
     * @return ParseTree that results from parsing compiled file
     * @throws ParseException when there is an error parsing the file
     * @throws IOException    when there is an error reading the file.
     */
    public ParseTree parse(File file) throws ParseException, IOException {
        return parse(CharStreams.fromPath(file.toPath()));
    }

    /**
     * Compiles a given Simple Prog file into an SPRILL program.
     *
     * @param file PROG file to compile.
     * @return A program instance of this PROG file
     * @throws ParseException when the file can not be parsed.
     * @throws IOException    when the file can not be read.
     */
    public Program compile(File file) throws ParseException, IOException {
        return compile(parse(file));
    }

    /**
     * Compiles a given Simple Prog parse tree into an SPRILL program.
     *
     * @param tree the ParseTree of the program to compile.
     * @return A program instance of this PROG program
     */
    private Program compile(ParseTree tree) {
        Result checkResult = checker.check(tree);
        return generator.generate(tree, checkResult);
    }

    /**
     * Writes a program to the filesystem.
     *
     * @param p    The program that is to be written. Can be obtained as output by the compile method.
     * @param name The name of the output file. Will be placed in out/*Filename*
     */
    public void writeProgram(Program p, String name) {
        String dir = System.getProperty("user.dir");
        try {
            PrintWriter writer = new PrintWriter(dir + "/out/" + name + ".hs", "UTF-8");
            writer.println(p.getProgram());
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    /**
     * Will use the by ANTLR provided tools to parse a program and return it's tree
     *
     * @param chars A charStream of the provided program
     * @return A ParseTree of the provided program
     * @throws ParseException if the code is unparsable.
     */
    private ParseTree parse(CharStream chars) throws ParseException {
        ErrorListener listener = new ErrorListener();
        Lexer lexer = new GrammarLexer(chars);
        lexer.removeErrorListeners();
        lexer.addErrorListener(listener);
        TokenStream tokens = new CommonTokenStream(lexer);
        GrammarParser parser = new GrammarParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(listener);
        ParseTree result = parser.program();
        listener.throwException();
        return result;
    }
}
