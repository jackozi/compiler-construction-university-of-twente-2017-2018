/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package compiler;

import exceptions.ParseException;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

import java.util.ArrayList;
import java.util.List;

/**
 * This class (extension of {@link BaseErrorListener}) is a utility class for managing the error messages.
 */
public class ErrorListener extends BaseErrorListener {
    /**
     * Errors collected by the listener.
     */
    private final List<String> errors = new ArrayList<>();

    /**
     * Indicates if the listener has collected any errors.
     *
     * @return true if errors, false if none
     */
    public boolean hasErrors() {
        return !this.errors.isEmpty();
    }

    /**
     * Returns the (possibly empty) list of errors collected by the listener.
     *
     * @return A list of errors.
     */
    public List<String> getErrors() {
        return this.errors;
    }

    /**
     * Add an error to this error listener
     *
     * @param recognizer         Who found the error
     * @param offendingSymbol    What caused the error
     * @param line               Where is the error
     * @param charPositionInLine More specific please
     * @param msg                What can you tell us about the error
     * @param e                  What is the error
     */
    @Override
    public void syntaxError(Recognizer<?, ?> recognizer,
                            Object offendingSymbol, int line, int charPositionInLine,
                            String msg, RecognitionException e) {
        this.errors.add(String.format("line %d:%d - %s", line,
                charPositionInLine,
                msg));
    }

    /**
     * Throws an exception if any errors were reported;
     * does nothing otherwise.
     *
     * @throws ParseException An exception containing all errors found
     *                        during listening
     */
    public void throwException() throws ParseException {
        if (hasErrors()) {
            throw new ParseException(getErrors());
        }
    }
}
