/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package exceptions;

import java.util.List;

/**
 * Exception to be thrown when an error occurs during parsing.
 */
public class ParseException extends Exception {

    /**
     * List of errors.
     */
    private final List<String> messages;

    /**
     * Adds the provided messages to the list of exceptions
     *
     * @param messages messages to add to this class
     */
    public ParseException(List<String> messages) {
        super(messages.toString());
        this.messages = messages;
    }

    /**
     * Returns the error messages wrapped in this exception.
     *
     * @return list of errors
     */
    public List<String> getMessages() {
        return this.messages;
    }

    /**
     * Prints all error messages to stdout, line by line
     */
    public void print() {
        for (String error : getMessages()) {
            System.err.println(error);
        }
    }
}
