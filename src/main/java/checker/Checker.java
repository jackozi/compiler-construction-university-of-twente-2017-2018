/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package checker;

import grammar.GrammarBaseListener;
import grammar.GrammarParser.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.ArrayList;
import java.util.List;

/**
 * The (type) checker. This class visits all nodes,
 * sets the proper cfg-entries and checks if the code yields the proper types.
 */
public class Checker extends GrammarBaseListener {
    /**
     * Symbol Table that is used to track symbols and their types during type checking.
     */
    private final SymbolTable symbolTable = new SymbolTable();
    /**
     * Keeps tracks of the types of contexts
     */
    private final ParseTreeProperty<Type> contextTypes = new ParseTreeProperty<>();
    /**
     * Keeps track of thrown errors
     */
    private final List<String> errors = new ArrayList<>();
    /**
     * The result that will be updated during type checking.
     */
    private Result result;

    /**
     * Sets the proper cfg entry when exiting the program.
     *
     * @param ctx Context of the visited program.
     */
    @Override
    public void exitProgram(ProgramContext ctx) {
        setEntry(ctx, ctx);
    }

    /**
     * Sets the proper cfg entry and opens a new scope.
     *
     * @param ctx Context of the visited body.
     */
    @Override
    public void enterBody(BodyContext ctx) {
        setEntry(ctx, ctx);
        symbolTable.openScope();
    }

    /**
     * Closes the current scope after visiting of a body.
     *
     * @param ctx Context of the visited body.
     */
    @Override
    public void exitBody(BodyContext ctx) {
        symbolTable.closeScope();
    }

    /**
     * Checks if the embedded expression is of type BOOL and sets the cfg entry.
     *
     * @param ctx Context of the visited condition.
     */
    @Override
    public void exitCondition(ConditionContext ctx) {
        checkType(ctx.expr(), Type.BOOL);
        setEntry(ctx, entry(ctx.expr()));
    }

    /**
     * Checks if the embedded expression is of type BOOL and sets the cfg entry.
     *
     * @param ctx Context of the visited while statement.
     */
    @Override
    public void exitWhile(WhileContext ctx) {
        checkType(ctx.expr(), Type.BOOL);
        setEntry(ctx, entry(ctx.expr()));
    }

    /**
     * Sets the proper cfg entry to the embedded expression.
     *
     * @param ctx Context of the visited while statement.
     */
    @Override
    public void exitPrint(PrintContext ctx) {
        setEntry(ctx, ctx.expr());
    }

    /**
     * Sets the proper cfg entry to the embedded body and opens a new scope.
     *
     * @param ctx Context of the visited parallel statement.
     */
    public void enterParallel(ParallelContext ctx) {
        setEntry(ctx, ctx.body());
        symbolTable.openScope();
    }

    /**
     * Increments the parallel count of the result and closes the current scope.
     *
     * @param ctx Context of the visited parallel statement.
     */
    @Override
    public void exitParallel(ParallelContext ctx) {
        result.setParallelCount(result.getParallelCount() + 1);
        symbolTable.closeScope();
    }

    /**
     * Checks if the embedded expression is of type BOOL and sets the cfg entry.
     *
     * @param ctx Context of the visited wait statement.
     */
    @Override
    public void exitWait(WaitContext ctx) {
        checkType(ctx.expr(), Type.BOOL);
        setEntry(ctx, ctx.expr());
    }

    /**
     * Checks if the id of the to be assigned variable already exists with the same type.
     * If the types do not match, add and error. If it does not exist, add it to the symbol table and type table
     *
     * @param ctx Context of the visited assignment.
     */
    @Override
    public void exitAssignment(AssignmentContext ctx) {
        String id = ctx.ID().getText();
        Type type = getType(ctx.expr());

        // Already exists in current scope and not proper type?
        Type exists = symbolTable.get(id);

        if (exists != null && exists != type && symbolTable.contains(id)) {
            addError(ctx, "Expected type '%s' but found '%s'", type, exists);
            setType(ctx, Type.ERROR);
        } else {
            symbolTable.put(id, type);
            setType(ctx.expr(), type);
        }
        setEntry(ctx, ctx);
    }

    /**
     * Works like {@link #exitAssignment},
     * but does not check and always overwrites the (potentially) existing variable.
     *
     * @param ctx Context of the visited assignment (init).
     */
    @Override
    public void exitInitAssignment(InitAssignmentContext ctx) {
        String id = ctx.ID().getText();
        Type type = getType(ctx.expr());

        symbolTable.put(id, type);

        setType(ctx.expr(), getType(ctx.expr()));
        setEntry(ctx, ctx);
    }

    /**
     * Sets the type to that of the embedded expression and adds a cfg entry.
     *
     * @param ctx Context of the visited prefix expression.
     */
    @Override
    public void exitPrefixExpr(PrefixExprContext ctx) {
        setType(ctx, getType(ctx.expr()));
        setEntry(ctx, ctx.expr());
    }

    /**
     * Sets the type to {@link Type.Int} and sets the cfg entry to the first embedded expression.
     *
     * @param ctx Context of the visited multiplication expression.
     */
    @Override
    public void exitMultExpr(MultExprContext ctx) {
        setType(ctx, Type.INT);
        setEntry(ctx, entry(ctx.expr(0)));
    }

    /**
     * Sets the type to {@link Type.Int} and sets the cfg entry to the first embedded expression.
     *
     * @param ctx Context of the visited plus expression.
     */
    @Override
    public void exitPlusExpr(PlusExprContext ctx) {
        setType(ctx, Type.INT);
        setEntry(ctx, ctx.expr(0));
    }

    /**
     * Sets the type to {@link Type.Error} if one of the embedded expressions is not of type {@link Type.Bool},
     * else to {@link Type.Bool}.
     * Also sets the cfg entry to the first embedded expression.
     *
     * @param ctx Context of the visited bool expression.
     */
    @Override
    public void exitBoolExpr(BoolExprContext ctx) {
        if (getErrors(ctx.expr(0), ctx.expr(1), "bool"))
            setType(ctx, Type.ERROR);
        else
            setType(ctx, Type.BOOL);
        setEntry(ctx, ctx.expr(0));
    }

    /**
     * Checks if the embedded expressions are of a type that can be compared sets the type to {@link Type.Bool}
     * if that is the case adds an error otherwise.
     * Also sets the cfg entry to the first embedded expression.
     *
     * @param ctx Context of the visited compare expression.
     */
    @Override
    public void exitCompExpr(CompExprContext ctx) {
        boolean error;
        if ((ctx.compOp().EQ() != null) || (ctx.compOp().NE() != null))
            error = getErrors(ctx.expr(0), ctx.expr(1), "bool", "int", "float");
        else
            error = getErrors(ctx.expr(0), ctx.expr(1), "float", "int");
        if (error)
            setType(ctx, Type.ERROR);
        else
            setType(ctx, Type.BOOL);
        setEntry(ctx, ctx.expr(0));
    }

    /**
     * Sets the type to that of the embedded expression and sets the cfg entry to the embedded expression.
     *
     * @param ctx Context of the visited parallel expression.
     */
    @Override
    public void exitParExpr(ParExprContext ctx) {
        setEntry(ctx, ctx.expr());
        setType(ctx, getType(ctx.expr()));
    }

    /**
     * Sets the type to {@link Type.Int} and sets the cfg entry.
     *
     * @param ctx Context of the visited number expression.
     */
    @Override
    public void exitNumExpr(NumExprContext ctx) {
        setType(ctx, Type.INT);
        setEntry(ctx, ctx);
    }

    /**
     * Checks if the visited id is in the symbol table.
     * If it is not, the type is set to {@link Type.Error} and an general error is added.
     * Otherwise, it sets the type to the type it already had (from the symbol table) and sets the cfg entry.
     *
     * @param ctx Context of the visited id expression.
     */
    @Override
    public void exitIdExpr(IdExprContext ctx) {
        String id = ctx.getText();
        Type type = symbolTable.get(id);
        if (!symbolTable.contains(id)) {
            setType(ctx, Type.ERROR);
            addGeneralError(String.format("Variable (%s) has not yet been declared in the current scope.", id), ctx);
        } else {
            setType(ctx, type);
            setEntry(ctx, ctx);
        }
    }

    /**
     * Sets the type to {@link Type.Bool} and sets the cfg entry.
     *
     * @param ctx Context of the visited true expression.
     */
    @Override
    public void exitTrueExpr(TrueExprContext ctx) {
        setType(ctx, Type.BOOL);
        setEntry(ctx, ctx);
    }

    /**
     * Sets the type to {@link Type.Bool} and sets the cfg entry.
     *
     * @param ctx Context of the visited false expression.
     */
    @Override
    public void exitFalseExpr(FalseExprContext ctx) {
        setType(ctx, Type.BOOL);
        setEntry(ctx, ctx);
    }

    // Helpers

    /**
     * Checks a parseTree and returns a result object with information about the program.
     *
     * @param p ParseTree of the Prog Program to check
     * @return a Result object containing information about the program.
     */
    public Result check(ParseTree p) {
        result = new Result();
        errors.clear();
        new ParseTreeWalker().walk(this, p);
        return result;
    }

    /**
     * Convenience method to add a type to the result.
     *
     * @param node The node to set type of
     * @param type Type to set.
     */
    private void setType(ParseTree node, Type type) {
        contextTypes.put(node, type);
    }

    /**
     * Convenience method to add a flow graph entry to the result
     *
     * @param node  node to set entry off.
     * @param entry entry node to set.
     */
    private void setEntry(ParseTree node, ParserRuleContext entry) {
        if (entry == null) {
            throw new IllegalArgumentException("Null flow graph entry");
        }
        result.setEntry(node, entry);
    }

    /**
     * Checks the inferred type of a given parse tree,
     * and adds an error if it does not correspond to the expected type.
     *
     * @param node     node to check
     * @param expected type you expect the node to be.
     */
    private void checkType(ParserRuleContext node, Type expected) {
        Type actual = getType(node);
        if (actual == null) {
            throw new IllegalArgumentException("Missing inferred type of " + node.getText());
        }
        if (!actual.equals(expected)) {
            addError(node, "Expected type '%s' but found '%s'", expected, actual);
        }
    }

    /**
     * Records an error at a given parse tree node.
     *
     * @param node    the parse tree node at which the error occurred
     * @param message the error message
     * @param args    arguments for the message, see {@link String#format}
     */
    private void addError(ParserRuleContext node,
                          String message,
                          Object... args) {
        addError(node.getStart(), message, args);
    }

    /**
     * Records an error at a given token.
     *
     * @param token   the token at which the error occurred
     * @param message the error message
     * @param args    arguments for the message, see {@link String#format}
     */
    private void addError(Token token, String message, Object... args) {
        int line = token.getLine();
        int column = token.getCharPositionInLine();
        message = String.format(message, args);
        message = String.format("Line %d:%d - %s", line, column, message);
        errors.add(message);
    }

    /**
     * Gets the type of a node
     *
     * @param node node to get type of
     * @return the ype of the node.
     */
    private Type getType(ParseTree node) {
        return contextTypes.get(node);
    }

    private boolean getErrors(ParserRuleContext ctx1, ParserRuleContext ctx2, String... types) {
        boolean result = false;
        Type type1 = getType(ctx1);
        Type type2 = getType(ctx2);
        StringBuilder typesString = new StringBuilder();
        for (int i = 0; i < types.length; i++) {
            typesString.append(types[i]);
            if (i != types.length - 1)
                typesString.append(", ");
        }
        if (!isOneOf(type1.toString(), types)) {
            addTypeError(String.format("Found (%s), expected one of (%s)", type1, typesString.toString()), ctx1);
            result = true;
        }
        if (!isOneOf(type2.toString(), types)) {
            addTypeError(String.format("Found (%s), expected one of (%s)", type2, typesString.toString()), ctx2);
            result = true;
        }
        return result;
    }

    /**
     * This function returns whether the {@link String} type equals one of the types objects. The types parameter are the types
     * that need to be matched with type.
     *
     * @param type  String to be matched with types
     * @param types Strings to be matched with type
     * @return Whether one of types equals type
     */
    private boolean isOneOf(String type, String... types) {
        for (String t : types) {
            if (type.equals(t))
                return true;
        }
        return false;
    }

    /**
     * Adds an (type) error to the errors array. The error will consist of a {@link String} describing the error. It will also
     * display the line number and character number.
     *
     * @param description A String describing the error
     * @param context     Context of what went wrong, to get the line- and character position.
     */
    private void addTypeError(String description, ParserRuleContext context) {
        int line = context.getStart().getLine();
        int character = context.getStart().getCharPositionInLine();
        String error = String.format("Type error at %d:%d. %s ", line, character + 1, description);
        errors.add(error);
    }

    /**
     * Works like {@link #addTypeError}, but without the notion that it is a Type Error. Hence 'general' :).
     *
     * @param description A String describing the error.
     * @param context     Context of what went wrong, to get the line- and character position.
     */
    private void addGeneralError(String description, ParserRuleContext context) {
        int line = context.getStart().getLine();
        int character = context.getStart().getCharPositionInLine();
        String error = String.format("Error at %d:%d. %s ", line, character + 1, description);
        errors.add(error);
    }

    /**
     * Returns the flow graph entry of a given expression or statement.
     * <p>
     * Be careful when using this as the is required to have set the entry of the requested node first.
     *
     * @param node The net to get the entry of
     * @return the entry node of the the provided node.
     */
    private ParserRuleContext entry(ParseTree node) {
        return result.getEntry(node);
    }

    /**
     * Helper method to return if there are errors.
     *
     * @return Whether there are errors (if the ArrayList errors is empty, there are no errors).
     */
    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    /**
     * Helper method that returns the errors.
     *
     * @return The ArrayList of errors.
     */
    public List<String> getErrors() {
        return errors;
    }
}
