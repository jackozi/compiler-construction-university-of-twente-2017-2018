/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package checker;

/**
 * Type class that holds singletons of all types and the type representations.
 */
public class Type {
    /**
     * The singleton instance of the {@link Bool} type.
     */
    static final Type BOOL = new Bool();
    /**
     * The singleton instance of the {@link Int} type.
     */
    static final Type INT = new Int();
    /**
     * The singleton instance of the {@link Error} type.
     */
    static final Type ERROR = new Error();
    /**
     * Size in bytes of an Int
     */
    private static final int DEFAULT_SIZE = 4;
    /**
     * What are we dealing with
     */
    private final TypeKind kind;

    /**
     * Constructor for subclasses.
     *
     * @param kind The kind of type we're dealing with
     */
    public Type(TypeKind kind) {
        this.kind = kind;
    }

    /**
     * returns the size (in bytes) of a value of this type.
     *
     * @return the size in bytes of an Int
     */
    public int size() {
        return DEFAULT_SIZE;
    }

    /**
     * The supported types.
     */
    public enum TypeKind {
        // Integer base type.
        INT,
        // Boolean base type.
        BOOL,
        // Nullable base type.
        NIL,
        // Array compound type.
        ERROR
    }

    /**
     * Representation of the Prog Boolean type.
     */
    static public class Bool extends Type {
        private Bool() {
            super(TypeKind.BOOL);
        }

        /**
         * Size of a bool type a bool is currently 4 bytes so we can handle it
         * the same as an integer future optimisation could reduce this to 1 and store
         * multiple at the same memory address.
         *
         * @return Bytes of a bool
         */
        @Override
        public int size() {
            return 4;
        }

        /**
         * String representation of bool type.
         *
         * @return "bool"
         */
        @Override
        public String toString() {
            return "bool";
        }
    }

    /**
     * Representation of the Prog Error type.
     */
    static public class Error extends Type {
        private Error() {
            super(TypeKind.ERROR);
        }

        /**
         * Returns the size of an error type. This is currently
         * 0 as they will not have to be saved in memory ever.
         *
         * @return size of error type
         */
        @Override
        public int size() {
            return 0;
        }

        /**
         * Return the string representation of an error type.
         *
         * @return err
         */
        @Override
        public String toString() {
            return "err";
        }
    }

    /**
     * Representation of the Prog Integer type.
     */
    static public class Int extends Type {
        private Int() {
            super(TypeKind.INT);
        }

        /**
         * Returns the size of an int since the most common integer is an int32
         * we also use 4 bytes for an integer.
         *
         * @return the size of an integer type
         */
        @Override
        public int size() {
            return 4;
        }

        /**
         * String representation of an integer.
         *
         * @return "int"
         */
        @Override
        public String toString() {
            return "int";
        }
    }
}