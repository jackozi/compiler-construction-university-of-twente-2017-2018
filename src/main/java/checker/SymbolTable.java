/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package checker;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * This SymbolTable class is used in the type checking phase.
 */
class SymbolTable {
    /**
     * This stack will keep track of all scopes.
     */
    private final Stack<Map<String, Type>> stack = new Stack<>();

    /**
     * Constructor that gives you the first scope for free.
     */
    SymbolTable() {
        stack.add(new HashMap<>());
    }

    /**
     * Opens a new scope which will be the new top level scope.
     */
    void openScope() {
        HashMap<String, Type> map = new HashMap<>();
        stack.push(map);
    }

    /**
     * Closes the top level scope
     *
     * @throws RuntimeException if there is no scope to close
     */
    void closeScope() {
        if (stack.size() == 1)
            throw new RuntimeException();
        stack.pop();
    }

    /**
     * Puts element in the top scope. Using the id as key and type as value.
     *
     * @param id   ID of variable
     * @param type Type of variable
     * @return Whether the operation succeeded.
     */
    boolean put(String id, Type type) {
        return stack.peek().put(id, type) == null;
    }

    /**
     * Gets the type of id out of the stack. It will traverse bottom up.
     *
     * @param id Id of variable
     * @return Type of variable
     */
    Type get(String id) {
        for (Map<String, Type> scope : stack) {
            Type val = scope.get(id);
            if (val != null)
                return val;
        }
        return null;
    }

    /**
     * Returns whether any of the maps in the stack contain id as a key. Traverses the stack
     * bottom up.
     *
     * @param id Key to check
     * @return Key exists in the map
     */
    boolean contains(String id) {
        for (Map<String, Type> scope : stack) {
            if (scope.keySet().contains(id))
                return true;
        }
        return false;
    }

}