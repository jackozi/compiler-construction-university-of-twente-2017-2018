/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package checker;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

/**
 * This result object is created and filled during type checking and then handed over to the generator.
 */
public class Result {
    /**
     * Mapping from statements and expressions to the atomic
     * subtree that is their entry in the control flow graph.
     */
    private final ParseTreeProperty<ParserRuleContext> entries = new ParseTreeProperty<>();
    /**
     * Mapping from expressions to types.
     */
    private final ParseTreeProperty<Type> types = new ParseTreeProperty<>();
    /**
     * Mapping from variables to coordinates.
     */
    private final ParseTreeProperty<Integer> offsets = new ParseTreeProperty<>();
    /**
     * Amount of 'parallel' statements.
     */
    private int parallelCount;

    /**
     * Adds an association from parse tree node to the flow graph entry.
     *
     * @param node Node to set the entry for.
     * @param entry Entry point to set.
     */
    public void setEntry(ParseTree node, ParserRuleContext entry) {
        entries.put(node, entry);
    }

    /**
     * Returns the flow graph entry associated
     * with a given parse tree node.
     *
     * @param node Node to get entry point from.
     * @return The entry point from provided node.
     */
    public ParserRuleContext getEntry(ParseTree node) {
        return entries.get(node);
    }

    /**
     * Used to get how many parallel blocks are in the program so we can reserve enough space in the branching part.
     *
     * @return Amount of parallel blocks in the program.
     */
    public int getParallelCount() {
        return parallelCount;
    }

    /**
     * Sets the amount of parallels in the program so it can be retrieved later.
     *
     * @param parCount The amount of parallel blocks.
     */
    public void setParallelCount(int parCount) {
        this.parallelCount = parCount;
    }

}