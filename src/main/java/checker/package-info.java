/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

/**
 * Contains all checker related code (the checker, Result, SymbolTable and Type classes).
 */
package checker;