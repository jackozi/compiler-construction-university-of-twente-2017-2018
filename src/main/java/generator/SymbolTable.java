/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package generator;

import org.antlr.v4.runtime.misc.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * A symbol table keeping track of variables used and their location,
 * in order for this to function correctly a program should first be type checked.
 */
public class SymbolTable {
    /**
     * The symbol table works by keeping a stack with a map of values to their memory locations.
     */
    private Stack<Map<String, Integer>> stack = new Stack<>();

    /**
     * Constructor of a symbol table. Provides the first scope for free.
     */
    SymbolTable() {
        openScope();
    }

    /**
     * Opens a new scope needs to be called when entering one.
     */
    void openScope() {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("staticLink", 0);
        stack.push(map);
    }

    /**
     * Closes the top level scope needs to be called when leaving one.
     */
    void closeScope() {
        if (stack.size() == 1)
            throw new RuntimeException();
        stack.pop();
    }

    /**
     * Puts a variable on the symbol table.
     *
     * @param id     ID of the symbol
     * @param offset Memory offset of beginning of global memory
     * @return True if able to put it, false if it fails.
     */
    boolean put(String id, Integer offset) {
        return stack.peek().put(id, offset) == null;
    }

    /**
     * Gets the integer id and memory offset in a pair
     *
     * @param id The id of the symbol to get the values of
     * @return A integer pair containing the symbol id and memory offset.
     */
    Pair<Integer, Integer> get(String id) {
        for (int j = stackSize() - 1; j >= 0; j--) {
            Map<String, Integer> scope = stack.get(j);
            Integer val = scope.get(id);
            if (val != null)
                return new Pair<>(j, val);
        }
        return null;
    }

    /**
     * Finds how many scopes are currently open.
     *
     * @return This many scopes are open.
     */
    int stackSize() {
        return stack.size();
    }

    /**
     * How many elements are in the most top scope?
     *
     * @return this many elements are in the top scope.
     */
    int currentScopeSize() {
        return stack.peek().size();
    }

    /**
     * How many elements are in all scopes combined?
     *
     * @return this many elements are in all scopes combined!
     */
    int totalScopeSize() {
        int i = 0;
        for (Map<String, Integer> m : stack) {
            i = i + m.size();
        }
        return i;
    }

    /**
     * Checks if a given id is in any level symbol table.
     * <p>
     * Runs from most top to most bottom symbol table.
     *
     * @param id ID of the symbol to check.
     * @return true if it is found otherwise false.
     */
    public boolean contains(String id) {
        for (Map<String, Integer> scope : stack) {
            if (scope.keySet().contains(id))
                return true;
        }
        return false;
    }
}
