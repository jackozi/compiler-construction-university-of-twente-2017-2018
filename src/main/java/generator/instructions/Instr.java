/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package generator.instructions;

/**
 * List of instructions in SPRILL
 */
public enum Instr {
    Const,
    Compute,
    Load,
    Store,
    WriteInstr,
    ReadInstr,
    TestAndSet,
    Receive,
    Branch,
    Jump,
    Push,
    Pop,
    Nop
}