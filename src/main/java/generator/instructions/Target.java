/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package generator.instructions;

public class Target implements Argument {
    /**
     * Kind of target eg rel or abs
     */
    private final TargetCode code;
    /**
     * Otherwise the value to target
     */
    private Integer val;

    /**
     * Constructor with constant
     *
     * @param code Which type of jump operation to execute (e.g. Ind, Rel or Abs)
     * @param val  the integer to address
     */
    public Target(TargetCode code, int val) {
        this.code = code;
        this.val = val;
    }

    /**
     * This function is used to get the string that represents the object.
     *
     * @return representation in sprill
     */
    @Override
    public String toString() {
        return String.format("(%s (%s))", code.toString(), val.toString());
    }

    /**
     * Class to represent the target code in sprill
     */
    public enum TargetCode {
        Abs,
        Rel,
        Ind;

        /**
         * Returns the string representation of the target code
         *
         * @return target code
         */
        @Override
        public String toString() {
            switch (this) {
                case Abs:
                    return "Abs";
                case Rel:
                    return "Rel";
                case Ind:
                    return "Ind";
                default:
                    break;
            }
            return super.toString();
        }
    }
}
