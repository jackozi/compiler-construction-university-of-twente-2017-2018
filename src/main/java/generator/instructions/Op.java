/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package generator.instructions;

/**
 * Class to represent an operation from Sprockell.
 */
public enum Op implements Argument {
    Add,
    Sub,
    Mul,
    Equal,
    NEq,
    GT,
    GE,
    LT,
    LE,
    And,
    Or,
    Xor,
    LShift,
    RShift;

    /**
     * Represents the operation as a string
     *
     * @return operation string
     */
    @Override
    public String toString() {
        switch (this) {
            case Add:
                return "Add";
            case Sub:
                return "Sub";
            case Mul:
                return "Mul";
            case Equal:
                return "Equal";
            case NEq:
                return "NEq";
            case GT:
                return "Gt";
            case GE:
                return "GtE";
            case LT:
                return "Lt";
            case LE:
                return "LtE";
            case And:
                return "And";
            case Or:
                return "Or";
            case Xor:
                return "Xor";
            case LShift:
                return "LShift";
            case RShift:
                return "RShift";
            default:
                break;
        }
        return super.toString();
    }
}