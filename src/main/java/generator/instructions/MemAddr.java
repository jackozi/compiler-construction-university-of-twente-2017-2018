/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package generator.instructions;

/**
 * Class to represent the memory address from Sprockell.
 */
public class MemAddr implements Argument {
    /**
     * Kind of memory code
     */
    private MemCode code;
    /**
     * If register based reg addr containing mem addr
     */
    private Reg reg;
    /**
     * If abs addr where is it?
     */
    private Integer val;
    /**
     * Special alliased mem addr like IO
     */
    private String con;

    /**
     * Constructor with reg addr
     *
     * @param code Which type op memory operation to execute (e.g. DirAddr or IndAddr)
     * @param reg the register the location is stored
     */
    public MemAddr(MemCode code, Reg reg) {
        this.code = code;
        this.reg = reg;
    }

    /**
     * Constructor with constant
     *
     * @param code Which type op memory operation to execute (e.g. DirAddr or IndAddr)
     * @param val the integer to address
     */
    public MemAddr(MemCode code, int val) {
        this.code = code;
        this.val = val;
    }

    /**
     * A constant string memory address this is useful to address aliases in the SPRIL
     * e.g. integerIO and charIO
     *
     * @param con The alias to address e.g. integerIO
     */
    public MemAddr(String con) {
        this.con = con;
    }

    /**
     * Returns the Memory address as a string
     *
     * @return Memory address as a string
     */
    @Override
    public String toString() {
        if (val != null) {
            return String.format("(%s (%s))", code.toString(), val.toString());
        } else if (con != null) {
            return con;
        } else {
            return String.format("(%s %s)", code.toString(), reg.toString());
        }
    }

    /**
     * Memory code
     */
    public enum MemCode implements Argument {
        Addr,
        Deref,
        IndAddr,
        DirAddr,
        ImmValue;

        /**
         * Returns the memcode as a string
         *
         * @return memcode as a string
         */
        @Override
        public String toString() {
            switch (this) {
                case Addr:
                    return "Addr";
                case Deref:
                    return "Deref";
                case IndAddr:
                    return "IndAddr";
                case DirAddr:
                    return "DirAddr";
                case ImmValue:
                    return "ImmValue";
                default:
                    break;
            }
            return super.toString();
        }
    }
}