/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package generator.instructions;

public class Int implements Argument {
    /**
     * Value of this int
     */
    private final int val;

    /**
     * Constructor for an Int Object
     *
     * @param i value to store
     */
    public Int(int i) {
        this.val = i;
    }

    /**
     * Prints string representation of integer
     *
     * @return Value of self
     */
    @Override
    public String toString() {
        return String.format("%d", val);
    }
}
