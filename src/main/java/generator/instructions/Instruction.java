/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package generator.instructions;

public class Instruction {
    /**
     * The instruction
     */
    private final Instr instr;
    /**
     * This instructions argument.
     */
    private final Argument[] arguments = new Argument[4];

    /**
     * Constructs an Instruction object
     *
     * @param instr Instruction
     * @param args It's arguments.
     */
    public Instruction(Instr instr, Argument... args) {
        this.instr = instr;
        for (int i = 0; i < args.length && i <= 3; i++) {
            arguments[i] = args[i];
        }
    }

    /**
     * This function returns the string that represents the instruction.
     * @return instructions string
     */
    @Override
    public String toString() {
        switch (this.instr) {
            case Const:
                return constToString((Int) arguments[0], (Reg) arguments[1]);
            case Compute:
                return computeToString((Op) arguments[0], (Reg) arguments[1], (Reg) arguments[2], (Reg) arguments[3]);
            case Load:
                return loadToString((MemAddr) arguments[0], (Reg) arguments[1]);
            case Store:
                return storeToString((Reg) arguments[0], (MemAddr) arguments[1]);
            case Branch:
                return branchToString((Reg) arguments[0], (Target) arguments[1]);
            case Jump:
                return jumpToString((Target) arguments[0]);
            case Push:
                return pushToString((Reg) arguments[0]);
            case Pop:
                return popToString((Reg) arguments[0]);
            case WriteInstr:
                return writeToString((Reg) arguments[0], (MemAddr) arguments[1]);
            case ReadInstr:
                return readToString((MemAddr) arguments[0]);
            case Nop:
                return nopToString();
            case TestAndSet:
                return testAndSetToString((MemAddr) arguments[0]);
            case Receive:
                return receiveToString((Reg) arguments[0]);
            default:
                return "";
        }
    }


    /**
     * The following function contain necessary string formatting expressions.
     *
     * @param reg Register to save the receive to
     * @return String with SPRILL command
     */
    private String receiveToString(Reg reg) {
        return String.format("Receive %s", reg.toString());
    }

    /**
     * The following function contain necessary string formatting expressions.
     *
     * @param addr Addr to test and set
     * @return String with SPRILL command
     */
    private String testAndSetToString(MemAddr addr) {
        return String.format("TestAndSet %s", addr.toString());
    }

    /**
     * The following function contain necessary string formatting expressions.
     *
     * @param val Value to save
     * @param reg Register to save the const to
     * @return String with SPRILL command
     */
    private String constToString(Int val, Reg reg) {
        return String.format("Const %s %s", val.toString(), reg.toString());
    }

    /**
     * The following function contain necessary string formatting expressions.
     *
     * @param reg  Register to write from
     * @param addr Addr to write to
     * @return String with SPRILL command
     */
    private String writeToString(Reg reg, MemAddr addr) {
        return String.format("WriteInstr %s %s", reg.toString(), addr.toString());
    }

    /**
     * The following function contain necessary string formatting expressions.
     *
     * @param addr Addr to read from
     * @return String with SPRILL command
     */
    private String readToString(MemAddr addr) {
        return String.format("ReadInstr %s", addr.toString());
    }

    /**
     * The following function contain necessary string formatting expressions.
     *
     * @param op Operation to compute
     * @param reg0 First Input
     * @param reg1 Second Input
     * @param reg2 Register to save output
     * @return String with SPRILL command
     */
    private String computeToString(Op op, Reg reg0, Reg reg1, Reg reg2) {
        return String.format("Compute %s %s %s %s", op.toString(), reg0.toString(), reg1.toString(), reg2.toString());
    }

    /**
     * The following function contain necessary string formatting expressions.
     *
     * @param memAddr MemAddr to load
     * @param reg Register to load to
     * @return String with SPRILL command
     */
    private String loadToString(MemAddr memAddr, Reg reg) {
        return String.format("Load %s %s", memAddr.toString(), reg.toString());
    }

    /**
     * The following function contain necessary string formatting expressions.
     *
     * @param reg Register containing value to store.
     * @param memAddr MemAddr to store to
     * @return String with SPRILL command
     */
    private String storeToString(Reg reg, MemAddr memAddr) {
        return String.format("Store %s %s", reg.toString(), memAddr.toString());
    }

    /**
     * The following function contain necessary string formatting expressions.
     *
     * @param reg register to check if branch should be done
     * @param target target to jump to
     * @return String with SPRILL command
     */
    private String branchToString(Reg reg, Target target) {
        return String.format("Branch %s %s", reg.toString(), target.toString());
    }

    /**
     * The following function contain necessary string formatting expressions.
     *
     * @param target Target to jump to
     * @return String with SPRILL command
     */
    private String jumpToString(Target target) {
        return String.format("Jump %s", target.toString());
    }

    /**
     * The following function contain necessary string formatting expressions.
     *
     * @param reg Register containing the value to push
     * @return String with SPRILL command
     */
    private String pushToString(Reg reg) {
        return String.format("Push %s", reg.toString());
    }

    /**
     * The following function contains necessary string formatting expressions.
     *
     * @param reg register to save the popped value
     * @return String with SPRILL command
     */
    private String popToString(Reg reg) {
        return String.format("Pop %s", reg.toString());
    }

    /**
     * The following function contain necessary string formatting expressions.
     *
     * @return String with SPRILL command
     */
    private String nopToString() {
        return "Nop";
    }
}