/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package generator.instructions;

/**
 * Class to represent registers from Sprockell.
 */
public enum Reg implements Argument {
    regA,
    regB,
    regC,
    regD,
    regE,
    regF,
    regSp,
    reg0,
    regPc,
    regSprId;

    /**
     * Returns the register as a string
     *
     * @return register as a string
     */
    @Override
    public String toString() {
        switch (this) {
            case regA:
                return "regA";
            case regB:
                return "regB";
            case regC:
                return "regC";
            case regD:
                return "regD";
            case regE:
                return "regE";
            case regF:
                return "regF";
            case regPc:
                return "regPC";
            case regSp:
                return "regSP";
            case reg0:
                return "reg0";
            case regSprId:
                return "regSprID";
            default:
                break;
        }
        return super.toString();
    }
}
