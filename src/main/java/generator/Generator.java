/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package generator;

import checker.Result;
import generator.instructions.*;
import grammar.GrammarBaseVisitor;
import grammar.GrammarParser.*;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;
import java.util.Vector;

import static generator.instructions.Instr.*;
import static generator.instructions.MemAddr.MemCode.*;
import static generator.instructions.Op.*;
import static generator.instructions.Op.GE;
import static generator.instructions.Op.GT;
import static generator.instructions.Op.LE;
import static generator.instructions.Op.LT;
import static generator.instructions.Reg.*;
import static generator.instructions.Target.TargetCode.Abs;
import static generator.instructions.Target.TargetCode.Rel;

/**
 * This class generates SPRILL from a PROG ParseTree
 */
public class Generator extends GrammarBaseVisitor {
    /**
     * The program that will be populated.
     */
    private Program program;
    /**
     * SymbolTable keeping track of variables
     */
    private SymbolTable symbolTable;
    /**
     * Used to store the start position of parallel regions
     */
    private final ArrayList<Integer> threadPositions = new ArrayList<>();
    /**
     * The amount of parallel regions to reserve branching space for.
     */
    private int parCount;

    /**
     * Generates SPRILL code for evaluating an expression
     *
     * @param program program to out SPRILL to
     * @param op      Operator to use for this expression
     */
    private static void generateExpression(Program program, Op op) {
        program.emit(Instr.Pop, Reg.regA);
        program.emit(Instr.Pop, Reg.regB);
        program.emit(Instr.Compute, op, Reg.regB, Reg.regA, Reg.regA);
        program.emit(Instr.Push, Reg.regA);
    }

    /**
     * Writes an expression to shared memory.
     *
     * Assumes a the value to push and address to push to are on top of the stack.
     *
     * @param program program to write too.
     */
    private static void generateRead(Program program) {
        program.emit(Pop, regB);
        program.emit(Pop, regA);
        program.emit(TestAndSet, new MemAddr(DirAddr, 0));
        program.emit(Receive, regC);
        program.emit(Branch, regC, new Target(Rel, 2));
        program.emit(Jump, new Target(Rel, -3));
        program.emit(WriteInstr, regA, new MemAddr(IndAddr, regB));
        program.emit(WriteInstr, reg0, new MemAddr(DirAddr, 0));

    }

    /**
     * Used at the end of IF statement to jump over the else part
     *
     * @param p     programm to add SPRILL to
     * @param lines Amount of lines the ELSE part is so we know how far to jump
     * @param index Place in the program this block needs to be placed.
     */
    private static void skipElse(Program p, int lines, int index) {
        Vector<Instruction> block = new Vector<>();

        emit(instr(Jump, new Target(Rel, lines)), block);

        p.emitBlock(block, index);
    }

    /**
     * Used to skip over a block if a certain condition is met. (e.g. for if or while conditionals)
     * <p>
     * Assumes the result of the conditional is on top of the stack.
     *
     * @param p     Program to add block to
     * @param lines Amount of lines to skip over (block length)
     * @param index Place in program to put this block.
     */
    private static void skip(Program p, int lines, int index) {
        Vector<Instruction> block = new Vector<>();

        emit(instr(Pop, regA), block);
        emit(instr(Compute, Equal, reg0, regA, regA), block);
        emit(instr(Branch, regA, new Target(Rel, lines)), block);

        p.emitBlock(block, index);
    }

    /**
     * Helper method to make instr for blocks more readable by being able to
     * omit the New keyword.
     *
     * @param instr     instr to generate
     * @param arguments arguments for instr
     * @return new instruction with given parameters.
     */
    private static Instruction instr(Instr instr, Argument... arguments) {
        return new Instruction(instr, arguments);
    }

    /**
     * Adds an instruction to an vector, usefull for creating SPRILL blocks.
     *
     * @param instruction   Instruction to add
     * @param vector        Vector to add instruction in.
     */
    private static void emit(Instruction instruction, Vector<Instruction> vector) {
        vector.add(instruction);
    }

    /**
     * Builds the initial code needed to run the program. Consisting of:
     *
     * Sends sprockell cores to the right parts of the program or lets them wait.
     *
     * Visits all the children and generates their code.
     *
     * @return null
     */
    @Override
    public Object visitProgram(ProgramContext ctx) {
        int index = program.reserve(3 + (parCount * 3));
        visitChildren(ctx);
        int lines = 0;
        for (int i = index + 1; i < program.program.size(); i++) {
            lines += program.countLines(i);
        }
        int threadLines = (parCount * 3);
        startThreadLoop(program, lines + 3 + threadLines, index);
        return null;
    }

    // Code block cleanup big method
    private void startThreadLoop(Program p, int endProg, int index) {
        Vector<Instruction> block = new Vector<>();

        emit(instr(Compute, Equal, regSprId, reg0, regA), block);
        emit(instr(Branch, regA, new Target(Rel, ((parCount * 3) + 2))), block);
        for (int i = 0; i < threadPositions.size(); i++) {
            int sprId = (i % 3) + 1;
            emit(instr(Load, new MemAddr(ImmValue, sprId), regA), block);
            emit(instr(Compute, Equal, regSprId, regA, regA), block);
            emit(instr(Branch, regA, new Target(Abs, threadPositions.get(i))), block);
        }
        emit(instr(Jump, new Target(Abs, endProg)), block);

        p.emitBlock(block, index);
    }

    /**
     * Reserves a line in which sprockells not meant to run this fork get a jump statement to jump over it. This will be filled after the length of the generated code of the children is known.
     * <p>
     * Then visits the children of the fork statement and generates their code.
     * <p>
     * At the end return the sprockell to after where it jumped to this fork.
     *
     * @return null
     */
    @Override
    public Object visitParallel(ParallelContext ctx) {
        int index = program.reserve(1);
        symbolTable.openScope();
        visitChildren(ctx);
        symbolTable.closeScope();
        int lines = 0;
        for (int i = index + 1; i < program.program.size(); i++)
            lines += program.countLines(i);
        int currentLine = 0;
        for (int i = 0; i <= index; i++)
            currentLine += program.countLines(i);
        threadPositions.add(currentLine);
        startFork(program, lines + 2, index);
        int begin = (2 + threadPositions.size() * 3);
        endFork(program, begin);
        return null;
    }

    /**
     * Code that let non targeted sprockells jump over this block
     *
     * @param p     Program to add block to
     * @param lines Amount of lines the block is
     * @param index Place to add the block
     */
    private void startFork(Program p, int lines, int index) {
        Vector<Instruction> block = new Vector<>();
        emit(instr(Jump, new Target(Rel, lines)), block);
        p.emitBlock(block, index);
    }

    /**
     * Lets the sprockell jump back to the threadpool
     *
     * @param p     Program to add block
     * @param begin Line to jump back to.
     */
    private void endFork(Program p, int begin) {
        Vector<Instruction> block = new Vector<>();
        emit(instr(Jump, new Target(Abs, begin)), block);
        p.emitBlock(block);
    }

    /**
     * When visiting a body nothing more is done then visiting it's children.
     *
     * @return null
     */
    @Override
    public Object visitBody(BodyContext ctx) {
        visitChildren(ctx);
        return null;
    }

    /**
     * Visits an assignment. Checks if it is already known and updates or if not known creates it.
     *
     * @return null
     */
    @Override
    public Object visitAssignment(AssignmentContext ctx) {
        visit(ctx.expr());

        String id = ctx.ID().getText();
        // Check if defined
        if (!symbolTable.contains(id))
            defineID(id);
            // else define it
        else
            updateID(id);

        return null;
    }

    /**
     * When a variable has been already declared in an earlier scope but needs to be reused as a new variable this command can recreate it in the local scope.
     * <p>
     * The previous scope will not be affected.
     *
     * @return null
     */
    @Override
    public Object visitInitAssignment(InitAssignmentContext ctx) {
        visit(ctx.expr());
        String id = ctx.ID().getText();

        defineID(id);

        return null;
    }

    /**
     * Generates code for an if statement. First reserves a block in which flow of the if statement is set up,
     * the expression of the if statement will be visited, a block that will evaluate that expression is generated and will jump to
     * if part or else part.
     * If part is generated, else part is generated.
     *
     * @return null
     */
    @Override
    public Object visitCondition(ConditionContext ctx) {

        visit(ctx.expr());
        int index = program.reserve(3);
        symbolTable.openScope();
        visitChildren(ctx.body(0));
        symbolTable.closeScope();
        int skipElse = program.reserve(1);

        int lines = 0;
        for (int i = index + 1; i < program.program.size(); i++) {
            lines += program.countLines(i);
        }
        int elseLines = 0;
        if (ctx.ELSE() != null) {
            symbolTable.openScope();
            visitChildren(ctx.body(1));
            symbolTable.closeScope();
            for (int i = skipElse + 1; i < program.program.size(); i++) {
                elseLines += program.countLines(i);
            }
        }
        skip(program, lines + 1, index);
        skipElse(program, elseLines + 1, skipElse);
        return null;
    }

    /**
     * Reserves a block which will evaluate the expression part and jump to end or just continue based on result
     *
     * then visits children to generate body
     *
     * @return null
     */
    @Override
    public Object visitWhile(WhileContext ctx) {
        int exprLen = program.reserve();
        visit(ctx.expr());
        symbolTable.openScope();
        int index = program.reserve(3);

        visit(ctx.body());
        symbolTable.closeScope();

        int currentLine = 0;
        for (int i = 0; i <= exprLen; i++)
            currentLine += program.countLines(i);
        int lines = 0;
        for (int i = index + 1; i < program.program.size(); i++) {
            lines += program.countLines(i);
        }
        skip(program, lines + 2, index);
        emit(Jump, new Target(Abs, currentLine));
        return null;
    }

    /**
     * Simple print, pops a number from the stack and prints it to standard out.
     *
     * @return null
     */
    @Override
    public Object visitPrint(PrintContext ctx) {
        visitChildren(ctx);
        emit(Pop, regA);
        emit(WriteInstr, regA, new MemAddr("numberIO"));
        return null;
    }

    /**
     * If prefix is - takes the number and multiplies with -1
     * if prefix is ! flips the bool value.
     *
     * @return null
     */
    @Override
    public Object visitPrefixExpr(PrefixExprContext ctx) {
        visitChildren(ctx);
        if (ctx.prefixOp().MINUS() != null) {
            emit(Instr.Load, new MemAddr(ImmValue, -1), regA);
            emit(Instr.Push, regA);
            generateExpression(program, Mul);
        } else {
            emit(Instr.Load, new MemAddr(ImmValue, 1), regA);
            emit(Instr.Push, regA);
            generateExpression(program, Xor);
        }
        return null;
    }

    /**
     * Generates a multiply expression on it's two children
     *
     * Should also be able to generate a devision but that is not yet supported.
     *
     * @return null
     */
    @Override
    public Object visitMultExpr(MultExprContext ctx) {
        visitChildren(ctx);

        if (ctx.multOp().STAR() != null) {
            generateExpression(program, Mul);
        }

        return null;
    }

    /**
     * Visits children of an addition and generates code to add or substract to according to operator.
     *
     * @return null
     */
    @Override
    public Object visitPlusExpr(PlusExprContext ctx) {
        visitChildren(ctx);

        if (ctx.plusOp().PLUS() != null) {
            generateExpression(program, Add);
        } else {
            generateExpression(program, Sub);
        }

        return null;
    }

    /**
     * Visits children of an boolean expression and generates code to compare to according to operator.
     *
     * @return null
     */
    @Override
    public Object visitBoolExpr(BoolExprContext ctx) {
        visitChildren(ctx);

        if (ctx.boolOp().AND() != null) {
            generateExpression(program, And);
        } else {
            generateExpression(program, Or);
        }

        return null;
    }

    /**
     * Visits children of an comparison and generates code to compare to according to operator.
     *
     * @return null
     */
    @Override
    public Object visitCompExpr(CompExprContext ctx) {
        visitChildren(ctx);

        CompOpContext comp = ctx.compOp();
        Op op;

        if (comp.NE() != null) {
            op = NEq;
        } else if (comp.LE() != null) {
            op = LE;
        } else if (comp.LT() != null) {
            op = LT;
        } else if (comp.GE() != null) {
            op = GE;
        } else if (comp.GT() != null) {
            op = GT;
        } else {
            op = Equal;
        }

        generateExpression(program, op);

        return null;
    }

    /**
     * Gets the value of a number expression and pushes it to the stack.
     *
     * @return null
     */
    @Override
    public Object visitNumExpr(NumExprContext ctx) {
        pushValue(ctx.getText());
        return null;
    }

    /**
     * Gets the value of an ID expr from the global memory.
     *
     * @return null
     */
    @Override
    public Object visitIdExpr(IdExprContext ctx) {
        String id = ctx.ID().getText();
        Pair<Integer, Integer> tuple = symbolTable.get(id);

        emit(Load, new MemAddr(ImmValue, tuple.b), regA);
        emit(ReadInstr, new MemAddr(IndAddr, regA));
        emit(Receive, regA);
        emit(Push, regA);

        return null;
    }

    /**
     * Pushes a TRUE value to the stack.
     *
     * @return null
     */
    @Override
    public Object visitTrueExpr(TrueExprContext ctx) {
        pushValue(1);
        return null;
    }

    /**
     * Pushes a FALSE value to the stack.
     *
     * @return null
     */
    @Override
    public Object visitFalseExpr(FalseExprContext ctx) {
        pushValue(0);
        return null;
    }

    /**
     * Waits for a certain condition to be met then continues.
     *
     * @return null
     */
    @Override
    public Object visitWait(WaitContext ctx) {
        int index = program.reserve();

        visit(ctx.expr());

        int lines = 0;
        for (int i = index + 1; i < program.program.size(); i++) {
            lines += program.countLines(i);
        }
        emit(Pop, regA);
        emit(Branch, regA, new Target(Rel, 2));
        emit(Jump, new Target(Rel, -(lines + 2)));

        return null;
    }

    // Updates the value of ID in symbol table
    private void updateID(String id) {
        pushValue(symbolTable.get(id).b);
        generateRead(program);
    }

    // Defines a value in the symbol table and assigns it a value
    private void defineID(String id) {
        int offset = symbolTable.totalScopeSize();
        symbolTable.put(id, offset);
        pushValue(offset);
        generateRead(program);
    }

    // Pushes a value to the stack
    private void pushValue(String value) {
        pushValue(Integer.parseInt(value));
    }

    // Pushes a value to the stack
    private void pushValue(int value) {
        emit(Instr.Load, new MemAddr(ImmValue, value), regA);
        emit(Push, regA);
    }

    /**
     * Generate a porgam from a Prog ParseTree and the Result from running it through the typechecker.
     *
     * @param parseTree ParseTree from Prog Program
     * @param result Result from type checking Prog program
     * @return Program containing SPRILL
     */
    public Program generate(ParseTree parseTree, Result result) {
        program = new Program();
        symbolTable = new SymbolTable();
        parCount = result.getParallelCount();
        parseTree.accept(this);
        return program;
    }

    /**
     * Returns the program generated by the generator
     *
     * @return the program
     */
    public Program getProgram() {
        return program;
    }


    // private emit function to clean up code, emits an instruction to this program
    private void emit(Instr instr, Argument... arguments) {
        program.emit(instr, arguments);
    }
}
