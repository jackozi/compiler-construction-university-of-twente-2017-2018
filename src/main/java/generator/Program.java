/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

package generator;

import generator.instructions.Argument;
import generator.instructions.Instr;
import generator.instructions.Instruction;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Vector;

/**
 * This is a data structure representing a PROG programs SPRILL code.
 */

public class Program {
    /**
     * List of string of SPRILL in this program
     */
    public final ArrayList<String> program;

    /**
     * Generates a new empty program.
     */
    public Program() {
        program = new ArrayList<>();
    }


    /**
     * Used to find the location the program is at, useful for returning here later.
     *
     * @return integer of line number current code generation will be placed.
     */
    public int reserve() {
        int size = program.size();
        program.add("");
        return size;
    }

    /**
     * Reserves a set of lines in the program that can be filled up later. This is useful when you can not yet know what to
     * put here e.g. before a loop or conditional statement.
     *
     * @param j The amount of lines to reserve.
     * @return the index of current place in the program
     */
    public int reserve(int j) {
        int size = program.size();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < j; i++) {
            result.append('\n');
        }
        program.add(result.toString());
        return size;
    }


    /**
     * Get the current program counter.
     *
     * @return The current program counter.
     */
    public int getPC() {
        int c = 0;
        for (int i = 0; i < program.size(); i++)
            c += countLines(i);
        return c;

    }


    /**
     * Counts the amount of lines used in the instruction block at a given index.
     *
     * @param index Index of block
     * @return Number of lines in block
     */
    public int countLines(int index) {
        String str = program.get(index);
        int count = 0;
        InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
        try {
            byte[] c = new byte[1024];
            count = 0;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        } catch (IOException ignored) {

        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    /**
     * Returns the program in string format so it can be written to a file or printed.
     *
     * @return the program in the format that should be written to file.
     */
    public String getProgram() {
        StringBuilder result = new StringBuilder();
        for (String s : program) {
            result.append(s);
        }
        result = new StringBuilder(result.toString().replace("\n", "\n      , "));
        result.append(
                "EndProg ]\n\n" + "main = run [prog, prog, prog, prog] \n" + "\n\n-- main_0: no debug information\nmain_0 = runWithDebugger noDebugger [prog,prog,prog,prog]\n\n\n-- main_1: shows all intermediate local states\nmain_1 = runWithDebugger (debuggerSimplePrint showLocalMem) [prog,prog,prog,prog]\nshowLocalMem :: DbgInput -> String\nshowLocalMem ( _ , systemState ) = show $ localMem $ head $ sprStates systemState\n\n\n-- main_2: shows local state only in case the sprockell writes to it\nmain_2 = runWithDebugger (debuggerPrintCondWaitCond showLocalMem doesLocalMemWrite never) [prog,prog,prog,prog]\ndoesLocalMemWrite :: DbgInput -> Bool\ndoesLocalMemWrite (instrs,st) = any isStoreInstr instrs\n    where\n        isStoreInstr (Store _ _) = True\n        isStoreInstr _           = False\n\n\n-- main_3: like main_2, but opens an TCP port and lets you talk to the debugger over that\nmain_3 = runWithDebuggerOverNetwork (debuggerPrintCondWaitCond showLocalMem doesLocalMemWrite never) [prog,prog,prog,prog]\n\nmain_4 = runWithDebugger (debuggerSimplePrintAndWait myShow) [prog,prog,prog,prog]\n");
        result.insert(0, "import Sprockell \n\nprog = [");
        return result.toString();
    }

    /**
     * Uses a StringBuilder to give a textual representation of a program.
     *
     * @return Program in sting format.
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (String s : program) {
            result.append(s);
        }
        return result.toString();
    }

    /**
     * Appends an instruction to the program
     *
     * @param instruction to add to the program.
     */
    public void emit(Instruction instruction) {
        String result = instruction.toString();
        program.add(result + '\n');
    }

    /**
     * Appends an instruction to the program array
     *
     * @param instr the instruction to add to the program
     * @param args list of arguments for this instruction.
     */
    public void emit(Instr instr, Argument... args) {
        String result = new Instruction(instr, args).toString();
        program.add(result + '\n');
    }

    /**
     * Appends a vector of instructions to the program array
     *
     * @param instructions Vector of instructions.
     */
    public void emitBlock(Vector<Instruction> instructions) {
        StringBuilder result = new StringBuilder();
        for (Instruction instruction : instructions) {
            result.append(instruction.toString()).append('\n');
        }
        program.add(result.toString());
    }

    /**
     * Puts a vector of instructions in the program array at index index.
     *
     * Useful when a block has been reserved for this by calling the reserve(int) method.
     *
     * @param instructions Vector of instructions
     * @param index Index to place this block.
     */
    public void emitBlock(Vector<Instruction> instructions, int index) {
        StringBuilder result = new StringBuilder();
        for (Instruction instruction : instructions) {
            result.append(instruction.toString()).append('\n');
        }
        program.set(index, result.toString());
    }
}

