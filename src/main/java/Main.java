/*
 * Copyright (c) 2018. This project is made for the Programming Paradigms module at the University of Twente.
 *
 * @author Dex Bleeker - s1460366
 * @author Jacko Zuidema - s1359592
 * @version 1.0
 */

import compiler.Compiler;
import generator.Program;

import java.io.File;

/**
 * The main way of running the compiler. Contains main method to compile (check and generate) a given program.
 */
public class Main {
    /**
     * Create reference to the compiler singleton.
     */
    private static final Compiler compiler = Compiler.instance();

    /**
     * The main way of running the compiler :)
     *
     * Takes a flag:
     *
     * -t for type checking
     * -c for compiling
     *
     * followed by the location of the program and the location for the result to be written.
     *
     * if no location is provided it will be printed to the stout
     *
     * @param args flags and files to compile.
     */
    public static void main(String[] args) {
        try {
            if (args.length >= 2) {
                File input = new File(args[1]);
                switch (args[0]) {
                    case "-c":
                        compiler.check(input);
                        Program prog = compiler.compile(input);
                        if (args.length >= 3)
                            compiler.writeProgram(prog, args[2]);
                        else
                            System.out.print(prog.getProgram());
                        break;
                    case "-t":
                        compiler.check(input);
                        break;
                    default:
                        System.out.println("Usage: \n" +
                                "     [-c|-t]      compile or typecheck a file\n" +
                                "     <filepath>   file to compile or check\n" +
                                "     <name>?      (optional) name to write output (in out/<name>.hs) \n" +
                                "                  if empty will output to stdout\n" +
                                "                  ignored during typechecking\n");
                        break;
                }

            } else {
                System.out.println("Usage: \n" +
                        "     [-c|-t]      compile or typecheck a file\n" +
                        "     <filepath>   file to compile or check\n" +
                        "     <name>?      (optional) path to write output (in out/<name>.hs) \n" +
                        "                  if empty will output to stdout\n" +
                        "                  ignored during typechecking\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
