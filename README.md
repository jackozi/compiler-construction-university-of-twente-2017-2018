Compiler Construction - Final project
===================================

* Dex Bleeker - s1460366
* Jacko Zuidema - s1359592

Importing our project
---------------------
Our project is set-up with Maven. This is to guarantee the same (build-)settings on every device and this should
come in handy during evaluation of our project. Just import our project as a Maven-project (with IntellIJ, Eclipse or command line).

(Eclipse has no direct Maven support, you need the `m2e` plugin.)

Important folders
-----------------
* Source code: `src/main/java/`
* Unit tests: `src/test/java/`
* Grammar: `src/main/java/grammar/`
* Output (generated and compiled Haskell): `out`

Running the compiler
--------------------
```
run java -jar prog.jar with the following commands
     [-c|-t]      compile or typecheck a file
     <filepath>   file to compile or check
     <name>?      (optional) path to write output (in out/<name>.hs)
                  if empty will output to stdout
                  ignored during type checking
```

Running the tests
-----------------
Our tests automatically execute the generated Haskell-code.

* To run them in IntellIJ or Eclipse, right-click the folder (`src/test/java`) and select 'Run \'Tests\''.
* To run them from command-line, execute `mvn -q -B verify`.

(If tests fail with a not found error it means the writebuffer was not yet closed so the test could not open the file for reading. In this case please try again and all should go well.)
